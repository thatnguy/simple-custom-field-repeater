<?php

	abstract class SCFR_Content {

		public $id = '';

		public $class = '';

		// fields which will be saved into DB.
		protected $fields = array();

		protected $repeater = null;

		public function __construct( $args = array() ) {

			foreach ( array( 'id', 'class' ) as $key ) {
				if (isset( $args[ $key ] ) ) {
					$this->$key = $args[ $key ];
				}
			}

			if ( isset($args['fields'] ) && is_array( $args['fields'] ) ) {
				$this->set_fields( $args['fields'] );
			}

		}

		protected function setup_path( $filepath ) {
			$this->dir_path = realpath( dirname( $filepath ) );

			$this->dir_uri = 'http' .( $this->is_ssl() ? 's' : '' ). '://' .str_replace( $_SERVER['DOCUMENT_ROOT'], $_SERVER['HTTP_HOST'], $this->dir_path );
		}

		protected function is_ssl() {
			if ( isset( $_SERVER['HTTP_X_FORWARDED_PROTO'] ) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https' )
				return true;

			if ( ! empty( $_SERVER['HTTPS'] ) && 'off' !== $_SERVER['HTTPS'] ||
				 ( isset( $_SERVER['SERVER_PORT'] ) && 443 === $_SERVER['SERVER_PORT'] )
			   ) return true;

			return false;
		}

		public function set_repeater( SCFR_Repeater $repeater ) {
			$this->repeater = $repeater;
		}

		public function set_fields( Array $fields ) {

			// default settings for fields.
			$default = array(
				'required' => true,
				'default'  => '',
				'sanitize' => true,
			);

			foreach ( $fields as $field_name => $settings ) {

				if ( is_array( $settings ) ) {
					$this->fields[ $field_name ] = wp_parse_args( $settings, $default );
				} else {
					$this->fields[ $settings ] = $default;
				}
			}
		}

		public function get_fields() {
			return $this->fields;
		}

		// enqueue content script.
		// This function is intended to be overridden.
		public function enqueue() {}

		public function get_json_data() {
			$json = array();

			$json['id']     = $this->id;
			$json['class']  = $this->class;
			$json['fields'] = $this->fields;

			return $json;
		}

		// must be defined in the child class.
		abstract protected function render( $fields, $post_id );
	}