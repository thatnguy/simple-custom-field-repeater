<?php

	class SCFR_Repeater {

		public $name = '';

		public $title = '';

		public $label = 'Section';

		public $class = '';

		public $max_repeat = 5;

		public $post_types = array( 'post' );

		public $caps = '';

		public $context = 'advanced';

		public $priority = 'default';

		public $post_id = 0;

		public $post_ids = array();

		public $content = null;

		public $key = '';

		public function __construct( $name, $args = array() ) {

			// repeater must have name defined.
			if ( ! isset( $name ) || $name === '' || ctype_space( $name ) ) {
				throw new Exception( 'Repeater must have a name.' );
			}

			$props = get_object_vars( $this );

			foreach ( $props as $key => $value ) {
				if ( isset( $args[ $key ] ) ) {
					$this->$key = $args[ $key ];
				}
			}

			$this->post_types = (array) $this->post_types;

			$this->name = sanitize_key( $name );

			$this->key = "scfr_{$this->name}";

		}

		public function register_content( SCFR_Content $content ) {

			$this->content = $content;

			$this->content->set_repeater( $this );
		}

		public function has_content() {
			return isset( $this->content ) && $this->content instanceof SCFR_Content;
		}

		public function enqueue() {
			// custom scripts
			do_action( "scfr_{$this->name}_enqueue_script" );

			// enqueue any content scripts.
			$this->content->enqueue();

		}

		public function render( $post, $metabox ) {

			$this->post_id = $post->ID;

			$datas = $this->get_meta_datas();

			$fields = $this->content->get_fields();
			$fields_datas = array();

			// we need atleast one section to start.
			if ( count( $datas ) < 1 ) {
				$datas = array(
					0 => array()
				);

				// we just use default values.
				foreach ( $fields as $field_name => $value ) {
					$datas[0][ $field_name ] = $value['default'];
				}
			}

			// render the nonce field so we can validate upon post save.
			wp_nonce_field( "scfr_{$this->name}_nonce", "scfr_{$this->name}" );

			?>
			<div class="scfr-repeater-nav">
				<ul class="repeater-nav-items">
				<?php foreach ( $datas as $order => $controls ): ?>
					<li class="repeater-nav-item" <?php echo $order == 0 ? 'aria-selected="true"' : ''; ?>>
						<a href="scfr-<?php echo $this->name; ?>-section-<?php echo $order+1; ?>"><?php echo $this->label; ?><span class="nav-item-count"><?php echo $order+1; ?></span>
						</a>
					</li>
				<?php endforeach; ?>
				</ul>
				<button class="repeater-add button" type="button">Add <?php echo $this->label; ?></button>
				<button class="repeater-remove button" type="button">Remove <?php echo $this->label; ?></button>
			</div>
			<div class="scfr-repeater-content">
			<?php foreach ( $datas as $order => $controls ): ?>
				<div class="scfr-repeater-section" id="scfr-<?php echo $this->name; ?>-section-<?php echo $order + 1; ?>" <?php echo $order == 0 ? 'aria-hidden="false"' : 'aria-hidden="true"'; ?>>
				<?php foreach ( $fields as $field_name => $settings ): ?>
					<?php
						$fields_datas[ $field_name ]['name'] = "{$this->key}_{$field_name}" . ( $order > 0 ? '-' . ( $order + 1 ) : '' );

						if ( isset( $controls[ $field_name ] ) ) {
							$fields_datas[ $field_name ]['value'] = $controls[ $field_name ];
						} else {
							$fields_datas[ $field_name ]['value'] = $settings['default'];
						}
					?>
				<?php endforeach; ?>
				<?php $this->content->render( $fields_datas, $this->post_id ); ?>
				</div>
			<?php endforeach; ?>
			</div>

			<?php
		}

		public function get_meta_datas() {

			$datas = json_decode( get_post_meta( $this->post_id, $this->key, true ), true );

			// filter $datas hook here.
			$datas = apply_filters( 'scfr_post_meta_data', $datas, $this->post_id );

			return ! $datas ? array() : $datas;
		}

		public function has_capabilities() {
			if ( $this->caps && ! current_user_can( $this->caps ) ) {
				return false;
			}

			return true;
		}

		public function save( $post_id ) {

			if ( ! $this->has_capabilities() ) {
				return;
			}

			// only save if we pass nonce verification.
			if ( ! isset( $_POST["scfr_{$this->name}"] ) || ! wp_verify_nonce( $_POST["scfr_{$this->name}"], "scfr_{$this->name}_nonce" ) ) {
				return;
			}

			if ( ! $this->post_id ) {
				$this->post_id = $post_id;
			}
			
			// we need to remove slashes for quotes
			$post_datas = $this->get_post_values();

			$meta_datas = $this->get_meta_datas();

			$len = count( $post_datas );
			if ( $len < 1 ) {

				if ( ! empty( $meta_datas ) ) {
					delete_post_meta( $post_id, $this->key );
				}
			} else {

				if ( $len > $this->max_repeat ) {
					$post_datas = array_slice( $post_datas, 0, $this->max_repeat );
				}
				
				update_post_meta( $post_id, $this->key, json_encode( $post_datas ) );
			}
		}

		public function get_post_values() {

			$datas = array();
			$fields = $this->content->get_fields();

			// filter out $_POST for only scfr-{repeater_name} fields
			$scfr_fields = array();
			foreach ( $_POST as $key => $value ) {
				if ( strpos( $key, "{$this->key}_") === 0 ) {
					$scfr_fields[ $key ] = $value;
				}

			}

			// +1 assumes we include the ending '_' character 
			$len = strlen( $this->key )+1;
			$order = $l = null;

			// this will hold all repeating fields that will be excluded 
			// because they dont meet requirements
			// if a `required` field was not set, we should ignore all other fields
			// in this repeater.
			$ignore_indexes = array();

			foreach ( $scfr_fields as $name => $value ) {
				$field_name = substr( $name, $len );

				// try to get the index (order) of the field
				// e.g. title-2 where 2 is the order field.
				$order = explode( '-', $field_name );
				$l = count( $order );

				// this is an order field if its at the end of field name and is a number
				if ( is_numeric( $order[ $l - 1 ] ) ) {
					// keep ref of order number.
					$i = $order[ $l - 1 ] - 1;

					array_pop( $order );

					$field_name = implode( '-', $order );
				} else {
					// since we are here which means we have no order field appended to the field name.
					// therefore index should be 0.
					$i = 0;
				}

				if ( isset( $fields[ $field_name ] ) ) {

					// an value was set for a required field goes to the ignore list.
					if ( $fields[ $field_name ]['required'] && $value === '' ) {
						$ignore_indexes[ $i ] = true;
					}

					if ( ! isset( $ignore_indexes[ $i ] ) ) {

						$data = $fields[ $field_name ]['sanitize'] 
							  ? wp_filter_nohtml_kses( $value )
	  						  : $value;

	  					// @issue: $_POST datas with quotes (', ") are escaped.
	  					// this adds unwanted backslashes.
	  					$data = stripslashes_deep( $data );

	  					$datas[$i][ $field_name ] = $data;
					}

				}
			}

			unset( $order );
			unset( $l );

			// we need to re-index the order if there are any gaps between the indexes 
			// and also exclude any ignored indexes.
			if ( count( $datas ) > 0 ) {
				$tmp = array();
				foreach ( $datas as $i => $values ) {
					if ( ! isset( $ignore_indexes[ $i ] ) ) {
						$tmp[] = $values;
					}
				}

				$datas = $tmp;

				unset( $tmp );
			}

			return $datas;
		}

		public function get_json_data() {
			$json = array();

			$json['name'] = $this->name;
			$json['title'] = $this->title;
			$json['label'] = $this->label;
			$json['limit'] = $this->max_repeat;
			$json['content'] = $this->content->get_json_data();

			return $json;
		}
		
	}
