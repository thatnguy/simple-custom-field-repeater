<?php

	// public helper

	function scfr_get_repeater( $repeater_name, $post_id = '' ) {

		// no post ID provided, then we use the current post ID.
		if ( '' === $post_id ) {
			$post_id = get_the_ID();
		}

		$post_id = (int) $post_id;

		$data = get_post_meta( $post_id, 'scfr_'.$repeater_name, true );

		return json_decode( get_post_meta( $post_id, 'scfr_'.$repeater_name, true ), true );
	}

	function scfr_get_repeaters( $post_id = '' ) {

		if ( '' === $post_id ) {
			$post_id = get_the_ID();
		}

		$post_id = (int) $post_id;

		$repeaters = array();

		foreach ( get_post_meta( $post_id, '' ) as $key => $value ) {
			// is a repeater ifkey prefixed with 'scfr'
			if ( 'scfr' === substr( $key, 0, 4 ) ) {
				$repeaters[ $key ] = json_decode( $value[0], true );
			}
		}

		return $repeaters;
	}