<?php

	class SCFR_Manager {

		public $version = '1.0.0';

		public $name = 'scfr';

		public $dir_path = '';

		public $dir_uri = '';

		public static $instance = null;

		// collection of repeater instances identified by repeater name.
		public $repeaters = array();

		public function __construct() {

			// setup path to plugin.
			$this->dir_path = apply_filters( 'scfr_dir_path', trailingslashit( plugin_dir_path( __FILE__ ) ) );
			$this->dir_uri = apply_filters(' scfr_dir_uri', trailingslashit( plugin_dir_url( __FILE__ ) ) );

			require_once( $this->dir_path . '/inc/repeater.class.php' );
			require_once( $this->dir_path . '/inc/content.class.php' );

			add_action( 'load-post.php', array( $this, 'setup' ) );
			add_action( 'load-post-new.php', array( $this, 'setup' ) );

			// init.
			add_action( 'init', array( $this, 'scfr_init' ) );
		}

		// setup hooks
		public function setup() {

			// add meta boxes
			add_action( 'add_meta_boxes', array( $this, 'render' ), 10, 2 );

			// save post
			add_action( 'save_post', array( $this, 'save' ) );

			// Load scripts and styles last.
			add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_scripts' ), 2, 999 );

		}

		public function scfr_init() {
			$instance = self::get_instance();

			do_action( 'scfr_init', $instance );
		}

		public function enqueue_scripts() {

			// Enqueue the main plugin script.
			wp_enqueue_script( 'scfr', $this->dir_uri . 'js/scfr.js', array(), '', true );

			// enqueue main style.
			wp_enqueue_style( 'scfr', $this->dir_uri . 'css/scfr.css' );


			// tell our repeaters to enqueue their own script if they have.
			foreach ( $this->repeaters as $repeater ) {
				$repeater->enqueue();
			}

			// localize main script with repeater data
			wp_localize_script( 'scfr', 'scfr_data', $this->get_json_data() );
		}

		public function has_repeater( $name ) {
			return isset( $this->repeaters[ $name ] );
		}

		public function register_repeater( $name, $args = array() ) {
			$repeater;

			if ( ! is_object( $name ) ) {
				$repeater = new SCFR_Repeater( $name, $args );
			} else if ( is_object( $name ) && ! $name instanceof SCFR_Repeater ) {
				throw new Exception( 'registering repeater must be an instance of SCFR_Repeater' );
			}

			$this->repeaters[ $name ] = $repeater;

			return $repeater;
		}

		public function unregister_repeater( $name ) {
			if ( $this->has_repeater( $name ) ) {
				unset( $this->repeaters[ $name ] );
			}
		}

		public function get_repeater( $name ) {
			return $this->has_repeater( $name ) ? $this->repeaters[ $name ] : false;
		}

		public function get_json_data() {
			// get repeaters info
			$json = array();

			foreach ( $this->repeaters as $repeater ) {
				$json[ $repeater->name ] = $repeater->get_json_data();
			}

			return $json;
		}

		public function render( $post_type, $post ) {

			foreach ( $this->repeaters as $repeater ) {
				if ( ! in_array( $post_type, $repeater->post_types ) || ! $repeater->has_capabilities() ) {
					continue;
				}

				// if repeater only renders with certain posts and current post ID is not one of them, move along.
				if ( count( $repeater->post_ids ) > 0 && ! in_array( $post->ID, $repeater->post_ids ) ) {
					continue;
				}

				// only add meta box if repeater have content.
				if ( $repeater->has_content() ) {

					$id = "scfr-{$repeater->name}";

					add_meta_box(
						$id,							// id
						$repeater->title,				// title
						array($repeater, 'render'),		// metabox content callback
						$post_type,						// screen to show the metabox
						$repeater->context,				// context within the screen where the box should display
						$repeater->priority
					);

					// add plugin main class so we can style the meta box however we want.
					add_filter( "postbox_classes_{$post_type}_{$id}", array( $this, 'add_plugin_class' ) );
				}
			}
		}

		public function add_plugin_class( $classes = array() ) {
			$class = 'scfr';

			if ( ! in_array( $class, $classes ) ) {
				$classes[] = sanitize_html_class( $class );
			}

			return $classes;
		}

		public function save( $post_id ) {

			if (	
				defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ||
				wp_is_post_autosave( $post_id ) ||
				wp_is_post_revision( $post_id )
			) { return; }

			foreach ( $this->repeaters as $repeater ) {
				$repeater->save( $post_id );
			}
		}

		public static function get_instance() {

			if ( is_null( self::$instance ) ) {
				self::$instance = new self;
			}

			return self::$instance;
		}

	}