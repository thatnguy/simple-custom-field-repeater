A WordPress plugin for developers to create repeating custom fields.

### When to use ###
* If you as a developer need to create custom fields which need to be repeated and you only opt for this functionality.
* content of the custom fields (html) will be created by you.

### Creating Repeating Custom field content ###

```
#!php
class Image_Slider extends SCFR_Content {

   public function __construct() {
      $this->setup_path( __FILE__ );
   }
  
   public function render( $fields, $post_id ) {
      ?><div class="scfr-row">
          <span class="scfr-label">Image</span>
          <input type="hidden" class="attachment-id" name="<?php echo $fields['image']['name']; ?>" />
        </div><?php
   }

}
```
The *render()* function is called when create/editing a post.
When rendering, all your fields will be available to be used. Each field details can be accessed using the field identifier you registered the field with.
### ###
* For the field name (the input unique identifier) use $fields[<field_identifier>]['name'].
* For field value that is either a default value you specified upon registering the field or
the value stored in DB use $fields[<field_identifier>]['value'].

### Registering Fields ###


```
#!php

function register_scfr_fields( $scfr ) {
	require_once get_template_directory() . '/inc/repeater-contents/image-slider.class.php';

	$image_slider = new Image_Slider();
	
	// register content fields.
	$image_slider->set_fields( array( 'image' => array( 'required' => true, 'default' => '' ) ) );
	
	// register a repeater for this custom field content.
	$image_slider_repeater = $scfr->register_repeater(
		'image-slider',
		array(
			'title' => 'Image Slider',
			'label' => 'Image',
			'post_type' => array( 'page' )
		)
	);

	// tell our new repeater to register the content for it.
	$image_slider_repeater->register_content( $image_slider );
}

add_action( 'scfr_init', 'register_scfr_fields' );
```
hook into *scfr_init* action hook to register your repeater and repeating content.
In this hook the repeater manager instance is available for you to register the repeaters.

### Registering Repeater Optional Arguments ###
* *title (String)* - A title for the meta box for a repeater
* *label (String)* - label of each repeater item
* *post_types (Array)* - list of post types this repeater is available for
* *post_ids (Array)* - list of post this repeater is available for.
* *max_repeat (Int)* - maximum number of repeaters.