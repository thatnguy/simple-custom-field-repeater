;( function( root, document, undefined ) {
	
	var rootClass = 'scfr',
		contents = {};


	var rootClassPrfx = 'scfr',
		metaboxes = {};

	var Metabox = function( id ) {

		var self = this;

		self.label = scfr_data[ id ].label;	//??
		self.limit = scfr_data[ id ].limit;

		self.sectionIDPrfx = rootClassPrfx + '-' + scfr_data[ id ].name + '-section';

		self.el = document.getElementById( rootClassPrfx + '-' + id );

		if ( ! self.el ) {
			throw new Error( 'Could not find Metabox element.' );
		}

		// section content.
		var sections = self.el.getElementsByClassName( rootClassPrfx + '-repeater-section' );
		if ( 0 === sections.length ) {
			throw new Error( 'Metabox needs atleast one section initially.' );
		}

		// keep a ref of the first section for future use.
		self.section = sections[0];
		self.sectionCount = sections.length;

		// metabox content and nav
		self.content = self.el.querySelector( '.' + rootClassPrfx + '-repeater-content' );
		self.nav = self.el.querySelector( '.' + rootClassPrfx + '-repeater-nav' );
		self.navMenu = self.nav.querySelector( '.repeater-nav-items' );

		// check if there is any nav item. if none we addNavItem

		// add events.
		self.addButton = self.el.querySelector( '.repeater-add' );
		self.removeButton = self.el.querySelector( '.repeater-remove' );

		null != self.addButton && self.addButton.addEventListener( 'click', self.onAddSection.bind( self.addButton, self ) );
		null != self.removeButton && self.removeButton.addEventListener( 'click', self.onRemoveSection.bind( self.removeButton, self ) );
		self.navMenu.addEventListener( 'click', self.onToggleSection.bind( self.navMenu, self ) );

		return self;
	};


	// events.

	Metabox.prototype.onAddSection = function( metabox, e ) {

		metabox.addSection();

		metabox.removeButton.disabled = false;

		if ( metabox.limit === metabox.sectionCount ) {
			this.disabled = true;
		}

		metabox.content.classList.remove( 'empty' );

		return false;
	}

	Metabox.prototype.onRemoveSection = function( metabox, e ) {

		var currSection = metabox.getSelectedSection();
		
		metabox.removeSection( currSection.id );

		if ( 0 === metabox.sectionCount ) {
			this.disabled = true;

			metabox.content.classList.add( 'empty' );
		}
		
		if ( metabox.limit > metabox.sectionCount ) {
			metabox.addButton.disabled = false;
		}

		return false;
	}

	Metabox.prototype.onToggleSection = function( metabox, e ) {

		// prevent link from redirecting.
		e.preventDefault();

		var tar = e.target;

		var isLink = function( node ) {
			return 1 === node.nodeType && 'A' === node.nodeName;
		}

		// if target is not a link then we find it in the children list.
		!isLink( tar ) && ( tar = tar.querySelector( 'a' ) );

		if ( tar ) {
			var id = tar.getAttribute( 'href' );
			metabox.selectSection( id );
			
		}

		return false;
	}

	Metabox.prototype.getSection = function( idx ) {
		var section;

		if ( 'string' === typeof idx ) {
			section = this.content.querySelector( '#' + idx ) || this.content.querySelector( '#' + this.sectionIDPrfx + '-' + (idx+1) );
		} else if ( 'number' === typeof idx ) {
			section = this.content.getElementsByClassName( rootClassPrfx + '-repeater-section' )[ idx - 1 ];
		}

		return section ? section : false;
	}

	Metabox.prototype.addSection = function() {
		var self = this;

		// add new Section.
		var newSection = document.createElement( 'div' );
		newSection.className = rootClassPrfx + '-repeater-section';
		newSection.id = this.sectionIDPrfx + '-' + (this.sectionCount+1).toString();
		newSection.innerHTML = this.section.innerHTML;

		// walk through nodes in section and update any element
		// with 'name' attribute appropriately so that dont clash with previous section.
		walk( newSection, function( node ) {

			var name;

			if ( 1 === node.nodeType && node.hasAttribute( 'name' ) ) {
				name = node.getAttribute( 'name' );

				node.setAttribute( 'name', node.getAttribute( 'name' ) + '-' + (self.sectionCount+1).toString() );
			}

			return true;
		} );

		// add new nav item linking to this section.
		var item = this.addNavItem( newSection.id );

		this.content.appendChild( newSection );

		this.sectionCount++;

		this.selectSection( this.sectionCount );

	}

	Metabox.prototype.removeSection = function( idx ) {
		var section = this.getSection( idx );

		if ( ! section ) {
			return false;
		}

		// remove nav item
		if ( ! this.removeNavItem( section.id ) ) {
			return false;
		}

		// remove section.
		var removed = section.parentNode.removeChild( section );

		this.sectionCount > 0 && this.sectionCount--;

		if ( 0 < this.sectionCount ) {

			// normalize.
			this.normalize();

			this.selectSection( this.sectionCount );
		}

		return removed;

	}

	Metabox.prototype.getSelectedSection = function() {

		var selected, navItems = this.navMenu.querySelectorAll( '.repeater-nav-item' );

		for ( var i = 0, l = navItems.length; i < l; i++ ) {
			selected = navItems[i].getAttribute( 'aria-selected' );

			if ( null != selected && "true" === selected ) {	// found it
				var link = navItems[i].querySelector( 'a' ),
					id   = link.getAttribute( 'href' );

				return this.content.querySelector( '#' + id );
			}
		}

		return false;
	}

	Metabox.prototype.selectSection = function( idx ) {

		var section = this.getSection( idx ),
			currSection = this.getSelectedSection();

		if ( ! section ) {
			return false;
		}

		var navItem = this.getNavItem( section.id );
		navItem.setAttribute( 'aria-selected', 'true' );
		section.setAttribute( 'aria-hidden', 'false' );

		// if there is one section then no point deselecting any sections.
		if ( 1 === this.sectionCount ) {
			return section;
		}

		if ( currSection ) {

			// the selecting section is already selected.
			if ( currSection == section ) {
				return section;
			}

			var currNavItem = this.getNavItem( currSection.id );

			// hide the current selected section.
			currSection.setAttribute( 'aria-hidden', 'true' );
			currNavItem.setAttribute( 'aria-selected', 'false' );
		}

	}

	Metabox.prototype.getNavItem = function( sectionId ) {
		var link = this.navMenu.querySelector( 'a[href="' + sectionId + '"]' );

		return null != link ? link.parentNode : false;
	}

	Metabox.prototype.addNavItem = function( sectionId ) {

		var item = document.createElement( 'li' ),
			link = document.createElement( 'a' ),
			count2Str = (this.sectionCount+1).toString(),
			text = '';

		if ( 'undefined' === typeof sectionId ) {
			sectionId = this.sectionIDPrfx + '-' + count2Str;
		}

		// setup link to content using sectionId
		link.setAttribute( 'href', sectionId );

		if ( this.label ) {
			text = this.label;
		}
		link.innerText = text;

		var count = document.createElement( 'span' );
		count.className = 'nav-item-count';
		count.innerText = count2Str;

		link.append( count );

		item.className = 'repeater-nav-item';
		item.appendChild( link );

		this.navMenu.appendChild( item );

		return item;

	}

	Metabox.prototype.isNavItem = function( nav ) {
		if ( ! nav instanceof Element ) {
			return false;
		}

		if ( 0 > nav.className.indexOf( 'repeater-nav-item' ) ) {
			return false;
		}

		return true;
	}

	Metabox.prototype.removeNavItem = function( sectionId ) {
		var navItem = this.getNavItem( sectionId );

		if ( ! navItem ) {
			return false;
		}

		return navItem.parentNode.removeChild( navItem );
	}

	Metabox.prototype.updateNavItem = function( nav, sectionId ) {

		// check if is a nav item.
		if ( ! this.isNavItem( nav ) ) {
			return;
		}

		var link = nav.querySelector( 'a' );

		if ( ! link ) {
			return;
		}

		link.setAttribute( 'href', sectionId );
		var count = link.querySelector( '.nav-item-count' ),
			idx   = sectionId.split( '-' );

		idx = parseInt( idx[ idx.length-1 ] );

		if ( count && !isNaN( idx ) ) {
			count.innerText = idx;
		}

		count && ( count.innerText = idx );

		return nav;
	}

	// re-index sections, control names and update nav items linking to new
	// re-indexed sections.
	Metabox.prototype.normalize = function() {

		var count = getChildCount( this.content, true ),
			section, skip = false;

		for ( var i = 1; i <= count ; i++ ) {
			section = this.getSection( i );

			walk( section, function( node ) {
				var name;

				if ( 1 === node.nodeType && node.hasAttribute( 'name' ) ) {
					name = node.getAttribute( 'name' );

					var segs = name.split( '-' ), idx = parseInt( segs[ segs.length-1 ] );

					// last segment is a number.
					if ( ! isNaN( idx ) ) {
						// since the current index matches the index in the name then we
						// dont need to do any changes.
						if ( i === idx ) {
							skip = true;
							return false;
						}

						segs.pop();

						name = segs.join( '-' ) + ( 1 === i ? '' : '-' + i );
					}

					node.setAttribute( 'name', name );
				}

				return true;
			}, section );

			if ( skip ) {
				skip = false;
				continue;
			}

			var newId = this.sectionIDPrfx + '-' + i,
				nav   = this.getNavItem( section.id );

			nav && this.updateNavItem( nav, newId );

			section.id = newId;
		}

	}

	// Ready, Set, Boom!

	var $metaboxes = document.getElementsByClassName( rootClassPrfx );
	for ( var i = 0, l = $metaboxes.length; i < l; i++ ) {

		var mb = $metaboxes[i],
			id = mb.id.replace( rootClassPrfx + '-', '' );

		var metabox = new Metabox( id );

		metaboxes[ mb.id ] = metabox;

	}

	// walk through `node` and `callback`.
	// walk stops when callback returns false.
	function walk( node, callback, boundary ) {
		boundary = boundary || node;
		callback = callback || function() { return true; }

		node = node.firstChild;

		while (node) {

			if ( ! callback.call( null, node ) ) {
				return node;
			}

			if ( node == boundary ) {
				return node;
			}

			// children.
			if ( node.firstChild ) {
				node = node.firstChild;
				continue;
			}

			// siblings.
			if ( node.nextSibling ) {
				node = node.nextSibling;
				continue;
			}

			if ( node.parentNode ) {
				while ( node = node.parentNode ) {

					if ( node == boundary ) {
						return node;
					}

					if ( node.nextSibling ) {
						node = node.nextSibling;
						break;
					}
				}
			}
		}

		return node;
	}

	function getFirstElementChild( node, last ) {
		last = typeof last !== 'undefined' ? last : false;

		var sibling = last ? 'previousSibling' : 'nextSibling';

		node = last ? node.lastChild : node.firstChild;
		while ( node && 1 !== node.nodeType ) {
			node = node[ sibling ];
		}

		return node;
	}

	function getPreviousElementSibling( node ) {
		var n = node.previousSibling;

		while ( n && 1 !== n.nodeType ) {
			n = n.previousSibling;
		}

		return n;
	}

	function getChildCount( node, onlyElement ) {
		
		onlyElement = typeof onlyElement !== 'undefined' ? onlyElement : false;

		// not an element
		if ( ! node instanceof Element ) {
			return false;
		}

		if ( ! onlyElement ) {
			return node.childNodes.length;
		}

		var n = node.firstChild, count = 0;
		while ( n ) {
			1 === n.nodeType && count++;

			n = n.nextSibling;
		}

		return count;
	}

} )( this, document );
