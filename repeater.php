<?php

/*
	Plugin Name: Simple Custom Field Repeater
	Plugin URI: bitbucket.org/thatnguy/scfr
	Description: Use this plugin to create a group of custom fields that can be repeated.
	Author: Phong Nguyen
	License: GPLv2 or later

	Copyright 2016 Phong Nguyen
*/

// restrict direct access to plugin.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

define( 'SCFR_INC_PATH', trailingslashit( plugin_dir_path( __FILE__ ) ) );

// public helper.
require_once SCFR_INC_PATH . '/inc/helper.php';

// admin access below.
if ( ! is_admin() ) {
	return;
}

if ( ! function_exists( 'scfr_loader' ) ) {

	function scfr_loader() {
		require_once SCFR_INC_PATH . 'repeater-manager.class.php';

		// initialize repeater manager.
		$repeater_manager = new SCFR_Manager();

	}
}

scfr_loader();